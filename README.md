# Bruno Klabuster


## Organisation Klassentreffen 2020

Die Seite ist bereits in's Archiv gewandert. Um sie zu erneut auf zu rufen folgendermaßen vorgehen:

1. Daten von [hier](https://www.magentacloud.de/lnk/BOimlbpX) herunterladen (Datei speichern). Das Passwort entspricht dem unserer Adress-Datenbank.
2. Archiv entpacken (extrahieren).
3. In dem neu entstandenen Ordner `bruno-klabuster` die Datei `README.html` aufrufen.
